<?php

  function algorithm( $n ) {
    $arr = array();
    $rows = '';

    for ( $i = 0; $i < $n; $i++ ) {
      $arr[$i] = [];

      for ( $j = 0; $j < $n; $j++ ) {
        $v = ( $j + 1 ) * ( $i + 1 );

        array_push( $arr[$i], $v );
      }

      $rows .= implode( '&nbsp;', $arr[$i] ) . '<br>';
    }

    return $rows;
  }

  echo algorithm(5);
