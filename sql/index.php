<?php require_once( 'model.php' ); ?>

<!doctype html><html>

<head>
  <title>SQL</title>
</head>

<body>
    <table class="table">
      <thead>
        <tr>
          <th>event_name</th>
          <th>ticket_class</th>
          <th>ticket_available</th>
        </tr>
      </thead>
      <tbody>

        <?php
          $soal_3 = soal_3();

          foreach ( $soal_3 as $item ) : ?>

            <tr>
              <td><?php echo $item['event_name']; ?></td>
              <td><?php echo $item['ticket_class']; ?></td>
              <td><?php echo $item['ticket_available']; ?></td>
            </tr>

        <?php endforeach; ?>
      </tbody>
    </table>
</body>
</html>
