<?php

  require_once('db_config.php');

  $db = mysqli_connect( $db_host, $db_username, $db_password, $db_db );

  function soal_3() {
    global $db;

    $data = [];

    $result = $db->query('SELECT * FROM event JOIN eventticket ON event.event_id = eventticket.event_id JOIN ticket ON eventticket.ticket_id = ticket.ticket_id');

    if ( $result ) {
      $rows = $result->fetch_object();

      foreach ( $result as $row ) {
        array_push( $data, $row );
      }
    }

    $soal_3 = $data;

    return $soal_3;
  }
