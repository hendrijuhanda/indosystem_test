<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return redirect()->route('form');;
});

Route::get('form', ['as' => 'form', 'uses' => 'GbController@create']);
Route::get('gallery', ['as' => 'gallery', 'uses' => 'GbController@index']);
Route::get('admin', ['as' => 'admin', 'uses' => 'GbController@admin_index']);

Route::post('post', ['as' => 'post', 'uses' => 'GbController@store']);

Route::get('delete/{id}', ['as' => 'delete', 'uses' => 'GbController@destroy']);
