<!doctype html><html>

<head>
  <title>Guestbook</title>
</head>

<body>
  <p>
    <b>Menu</b><br>
    <a href="{{ URL::route('form') }}">Form</a><br>
    <a href="{{ URL::route('gallery') }}">Gallery</a><br>
    <a href="{{ URL::route('admin') }}">Admin</a><br>
  </p>

  <b>Form</b><br>
  <form action="{{ URL::route('post') }}" method="post">
    {{ csrf_field() }}

    <p><input name="name" placeholder="Name"></p>
    <p><input name="address" placeholder="Address"></p>
    <p><input name="phone" placeholder="Phone"></p>
    <p><textarea name="note" placeholder="Note"></textarea></p>

    <br>

    <button type="submit">Submit</button>
  </form>
</body>

</html>
