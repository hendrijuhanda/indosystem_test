<!doctype html><html>

<head>
  <title>Guestbook</title>
</head>

<body>
  <p>
    <b>Menu</b><br>
    <a href="{{ URL::route('form') }}">Form</a><br>
    <a href="{{ URL::route('gallery') }}">Gallery</a><br>
    <a href="{{ URL::route('admin') }}">Admin</a><br>
  </p>

  <b>Gallery</b><br>

  @forelse ( $data as $item )
    <p style="border: #eee;">
      <div style="display: inline-block; width: 100px;">Name</div>: {{ $item->name }}<br>
      <div style="display: inline-block; width: 100px;">Note</div>: {{ $item->note }}<br>
    </p>
  @empty
    <p>{{ 'No Data.' }}</p>
  @endforelse
</body>

</html>
